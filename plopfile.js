module.exports =  plop => {
    plop.setGenerator('component', {
        description: 'Create a  component',
        // User input prompts provided as arguments to the template
        prompts: [
          {
            type: 'input',
            name: 'name',
            message: 'What is your component name?'
          },
          {
            type: 'checkbox',
            name: 'componentType',
            message: 'What type of component would you like?',
            choices: [
              { name: "A class component - a stateful class component with full lifecycle hooks", value: "comp" },
              { name: "A functional component - a simple component that just returns a render output and has no state", value: "fc"}
            ]
          },
        ],
        actions: (data) => {
          
        let actions = [
            {
                type: 'add',
                force: true,
                path: 'src/components/{{camelCase name}}/{{camelCase name}}.styles.scss',
                templateFile: 'plop-templates/ComponentStyles.scss.hbs',
            },
            {
                type: 'add',
                force: true,
                path: 'src/components/{{camelCase name}}/{{camelCase name}}.props.tsx',
                templateFile: 'plop-templates/ComponentProps.tsx.hbs',
            },
            {
                type: 'append',
                force: true,
                path: 'src/components/index.ts',
                pattern: `/* PLOP_INJECT_IMPORT */`,
                template: `export * from './{{camelCase name}}/{{camelCase name}}.component';`
            },
        ];
        if (data.componentType=="fc") {

            actions.push({
              type: 'add',
              force: true,
              path: 'src/components/{{camelCase name}}/{{camelCase name}}.component.tsx',
              templateFile: 'plop-templates/ComponentFunctional.tsx.hbs',
            });  

          }  else if (data.componentType=="comp") {

            actions.push({
              type: 'add',
              force: true,
              path: 'src/components/{{camelCase name}}/{{camelCase name}}.component.tsx',
              templateFile: 'plop-templates/ComponentClass.tsx.hbs',
            }); 

            actions.push({
              type: 'add',
              force: true,
              path: 'src/components/{{camelCase name}}/{{camelCase name}}.state.tsx',
              templateFile: 'plop-templates/ComponentState.tsx.hbs',
            });  

          } 
          
          return actions;
        }
      });

      plop.setGenerator('screen', {
        description: 'Create a sreen',
        prompts: [{
          type: 'input',
          name: 'name',
          message: 'What is your screen name?'
        }],
        actions: [
          {
            type: 'add',
            path: 'src/screens/{{camelCase name}}/{{name}}.screen.tsx',
            templateFile: 'plop-templates/Screen.tsx.hbs',
          },
          {
            type: 'add',
            path: 'src/screens/{{camelCase name}}/{{name}}.props.tsx',
            templateFile: 'plop-templates/ComponentProps.tsx.hbs',
          },
          {
            type: 'add',
            path: 'src/screens/{{camelCase name}}/{{name}}.state.tsx',
            templateFile: 'plop-templates/ComponentState.tsx.hbs',
          },
          {
            type: 'add',
            path: 'src/screens/{{camelCase name}}/{{name}}.styles.scss',
            templateFile: 'plop-templates/ComponentStyles.scss.hbs',
          },
          {
            type: 'append',
            path: 'src/screens/index.ts',
            pattern: `/* PLOP_INJECT_IMPORT */`,
            template: `export * from './{{camelCase name}}/{{name}}.screen'`
          },
          {
            type: 'append',
            path: 'src/routes/approuter.tsx',
            pattern: `/* PLOP_INJECT_IMPORT_SCREENS */`,
            template: `import { {{pascalCase name}}Screen } from '../screens'`
          },
          {
            type: 'append',
            path: 'src/routes/approuter.tsx',
            pattern: `{/* PLOP_INJECT_IMPORT_ROUTES */}`,
            template: `           <Route
            path="/{{camelCase name}}"
            exact
            component={ {{pascalCase name}}Screen }
            />`
          },
        ]

      });
};