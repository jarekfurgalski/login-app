import React, {Fragment} from 'react';
import {BrowserRouter as Router, Link, Switch, Route} from 'react-router-dom';


/* This is part of the generator templating, do not edit or remove... */
/* PLOP_INJECT_IMPORT_SCREENS */
import { HomeScreen } from '../screens'



export default function AppRouter() {
  return (
    <Router>
      <Switch>
        
        

        {/* This is part of the generator templating, do not edit or remove... */}
        {/* PLOP_INJECT_IMPORT_ROUTES */}
           <Route
            path="/"
            exact
            component={ HomeScreen }
            />
       

      </Switch>
    </Router>
  );
}
