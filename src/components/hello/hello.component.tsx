import React, { Component, Fragment } from 'react';
import { HelloProps } from './hello.props';
import { HelloState } from './hello.state';
import './hello.styles.scss';

export class Hello extends Component<HelloProps, HelloState>{

    // constructor(props: HelloWorldProps){
    // super(props);
    // }

    componentDidMount(){
    console.log('Component did mount');
    }

    componentWillUnmount(){
    console.log('Component will unmount');
    }

    render(){

    return(
    <Fragment>
        <h1>Hello</h1>
    </Fragment>
    )
    }
    }